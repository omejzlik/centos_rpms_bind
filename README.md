# BIND 9

[BIND (Berkeley Internet Name Domain)](https://www.isc.org/downloads/bind/doc/) is a complete, highly portable
implementation of the DNS (Domain Name System) protocol.

Internet Systems Consortium
([https://www.isc.org](https://www.isc.org)), a 501(c)(3) public benefit
corporation dedicated to providing software and services in support of the
Internet infrastructure, developed BIND 9 and is responsible for its
ongoing maintenance and improvement.

More details about upstream project can be found on their
[gitlab](https://gitlab.isc.org/isc-projects/bind9). This repository contains
only upstream sources and packaging instructions for
[Fedora Project](https://fedoraproject.org).

## Subpackages

The package contains several subpackages, some of them can be disabled on rebuild.

* **bind** -- *named* daemon providing DNS server
* **bind-utils** -- set of tools to analyse DNS responses or update entries (dig, host)
* **bind-doc** -- documentation for current bind, *BIND 9 Administrator Reference Manual*.
* **bind-license** -- Shared license for all packages but bind-export-libs.
* **bind-pkcs11** -- *named* daemon built with native PKCS#11 support. Can be disabled by `--without PKCS11`.
* **bind-libs** and **bind-libs-lite** -- Shared libraries used by some others programs
* **bind-devel** -- Development headers for libs.
* **bind-dlz-\*** -- Dynamic loadable [DLZ plugins](http://bind-dlz.sourceforge.net/) with support for external databases


## Optional features

* *GSSTSIG* -- Support for Kerberos authentication in BIND.
* *LMDB* -- Support for dynamic database for managing runtime added zones. Provides faster removal of added zone with much less overhead. But requires lmdb linked to base libs.
* *DLZ* -- Support for dynamic loaded modules providing support for features *bind-sdb* provides, but only small module is required.
